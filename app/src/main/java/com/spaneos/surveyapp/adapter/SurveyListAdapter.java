package com.spaneos.surveyapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.spaneos.surveyapp.R;
import com.spaneos.surveyapp.activity.LandingActivity;
import com.spaneos.surveyapp.model.Survey;

import java.util.List;

/**
 * Created by spaneos on 7/6/16.
 */
public class SurveyListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<Survey> data;
    private ViewHolder holder;
    private Activity activity;

    private class ViewHolder{

        TextView value;
    }

    //gets the Context and list of roles which were getting from server
    public SurveyListAdapter(Context c,List<Survey> surveyList,Activity activity){
        context=c;
        data=surveyList;
        this.activity=activity;
    }



    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Survey pojo=data.get(position);

        holder=new ViewHolder();
        if(convertView == null){
            inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.survey_list_adapter,parent,false);

            holder.value=(TextView) convertView.findViewById(R.id.surveyTitle);
            convertView.setTag(holder);


        }
        else{
            holder=(ViewHolder)convertView.getTag();
        }

        if(pojo!=null){
            holder.value.setText(pojo.getTitle());
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((LandingActivity) activity).getSelectedSurvey(pojo);

            }
        });

        return convertView;
    }

}
