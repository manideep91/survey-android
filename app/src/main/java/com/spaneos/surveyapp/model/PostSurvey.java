package com.spaneos.surveyapp.model;

/**
 * Created by spaneos on 7/6/16.
 */
public class PostSurvey {

    private String surveyID;

    public PostSurvey(String surveyID) {
        this.surveyID = surveyID;
    }

    public String getSurveyID() {
        return surveyID;
    }

    public void setSurveyID(String surveyID) {
        this.surveyID = surveyID;
    }
}
