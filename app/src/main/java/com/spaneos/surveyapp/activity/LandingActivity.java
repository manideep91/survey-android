package com.spaneos.surveyapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;


import com.spaneos.surveyapp.R;
import com.spaneos.surveyapp.adapter.SurveyListAdapter;
import com.spaneos.surveyapp.connection.ApiClient;
import com.spaneos.surveyapp.connection.ApiInterface;
import com.spaneos.surveyapp.model.Survey;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by spaneos on 6/6/16.
 */
public class LandingActivity extends Activity {

    private static final String TAG = LandingActivity.class.getSimpleName();
    private ListView surveyListView;
    public static String selectedSurveyId="";
    public static String selectedSurveyName="";
    public List<Survey> surveyList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        surveyListView= (ListView) findViewById(R.id.listView);

        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);

        Call<List<Survey>> call=apiService.getMovieDetails();


        call.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                 surveyList = response.body();

                if(surveyList!=null && surveyList.size()>0) {
                    Log.d(TAG, "Number of surveys received: " + surveyList.size());
                    SurveyListAdapter adapter = new SurveyListAdapter(LandingActivity.this, surveyList, LandingActivity.this);
                    surveyListView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                Log.d(TAG,t.getMessage());
                Log.d(TAG, "API call failed");
            }
        });


        




    }

    public void getSelectedSurvey(Survey survey){

            selectedSurveyId = survey.getId();
            selectedSurveyName = survey.getTitle();
            Intent intent = new Intent(LandingActivity.this, SurveyQuestionsActivity.class);
            startActivity(intent);


    }
}
