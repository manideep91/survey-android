package com.spaneos.surveyapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by spaneos on 7/6/16.
 */
public class Survey {

    @SerializedName("_id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("category")
    private String category;

    @SerializedName("isPublished")
    private boolean isPublished;

    @SerializedName("creationDate")
    private String creationDate;

    //constructor
    public Survey(String id, String title, String category, boolean isPublished, String creationDate) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.isPublished = isPublished;
        this.creationDate = creationDate;
    }

    //setters and getters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean published) {
        isPublished = published;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
}
