package com.spaneos.surveyapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by spaneos on 7/6/16.
 */
public class QuestionResponse {

    /*{
        "questionId":"57557c12605b2e0300f2830e",
            "selectedOptions":[
        {
            "optionId":"57557c12605b2e0300f28310"
        }
        ]
    }*/

    @SerializedName("questionId")
    private String questionId;

    @SerializedName("selectedOptions")
    private List<SelectedOptions> selectedOptions;

    public QuestionResponse(String questionId, List<SelectedOptions> selectedOptions) {
        this.questionId = questionId;
        this.selectedOptions = selectedOptions;
    }

    public QuestionResponse(){

    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<SelectedOptions> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(List<SelectedOptions> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    @Override
    public String toString() {
        return "QuestionResponse{" +
                "questionId='" + questionId + '\'' +
                ", selectedOptions=" + selectedOptions +
                '}';
    }


}
