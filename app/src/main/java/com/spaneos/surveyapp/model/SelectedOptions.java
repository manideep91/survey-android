package com.spaneos.surveyapp.model;

/**
 * Created by spaneos on 7/6/16.
 */
public class SelectedOptions {

    private String optionId;

    public SelectedOptions(String optionId) {
        this.optionId = optionId;
    }

    public SelectedOptions(){

    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    @Override
    public String toString() {
        return "SelectedOptions{" +
                "optionId='" + optionId + '\'' +
                '}';
    }
}
