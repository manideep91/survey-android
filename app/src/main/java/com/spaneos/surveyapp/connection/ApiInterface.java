package com.spaneos.surveyapp.connection;

import com.spaneos.surveyapp.model.PostSurvey;
import com.spaneos.surveyapp.model.Question;
import com.spaneos.surveyapp.model.QuestionResponse;
import com.spaneos.surveyapp.model.Survey;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Manideep on 7/6/16.
 */
public interface ApiInterface {



    @GET("getAllSurveys")
    Call<List<Survey>> getMovieDetails();

    @POST("getQuestionsOfSurvey")
    Call<List<Question>> getQuestionsOfSurvey(@Body PostSurvey postSurvey);

    @PUT("updateQuestionResponse")
    Call<Question> updateQuestionResponse(@Body QuestionResponse questionResponse);


}
