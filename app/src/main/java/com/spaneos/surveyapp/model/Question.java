package com.spaneos.surveyapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by spaneos on 7/6/16.
 */
public class Question {

    @SerializedName("_id")
    private String id;

    @SerializedName("question")
    private String question;

    @SerializedName("creationDate")
    private String creationDate;

    @SerializedName("type")
    private boolean type;

    @SerializedName("options")
    private List<Options> optionsList;

    //constructor
    public Question(String id, String question, String creationDate, boolean type, List<Options> optionsList) {
        this.id = id;
        this.question = question;
        this.creationDate = creationDate;
        this.type = type;
        this.optionsList = optionsList;
    }

    //setters and getters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public List<Options> getOptionsList() {
        return optionsList;
    }

    public void setOptionsList(List<Options> optionsList) {
        this.optionsList = optionsList;
    }
}
