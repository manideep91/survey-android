package com.spaneos.surveyapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by spaneos on 7/6/16.
 */
public class Options {

    /*"option": "YES",
            "_id": "575654f20945d90300b5f0ad"*/

    @SerializedName("_id")
    private String id;

    @SerializedName("option")
    private String option;

    @SerializedName("resultCount")
    private int resultCount;

    public Options(String id, String option, int resultCount) {
        this.id = id;
        this.option = option;
        this.resultCount = resultCount;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }
}
