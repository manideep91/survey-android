package com.spaneos.surveyapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.spaneos.surveyapp.R;
import com.spaneos.surveyapp.connection.ApiClient;
import com.spaneos.surveyapp.connection.ApiInterface;
import com.spaneos.surveyapp.model.PostSurvey;
import com.spaneos.surveyapp.model.Question;
import com.spaneos.surveyapp.model.QuestionResponse;
import com.spaneos.surveyapp.model.SelectedOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by spaneos on 7/6/16.
 */
public class SurveyQuestionsActivity extends Activity {


    private TextView totalVotesTV;
    private TextView questionTV;
    private TextView optionOneResultTV;
    private TextView optionTwoResultTV;
    private Button surveyNameBtn;
    private Button submitBtn;
    private Button nextBtn;
    private RadioGroup radioOptions;
    private RadioButton radioOption1;
    private RadioButton radioOption2;
    private List<Question> questionsList;
    private int count=0;
    double optTwoPercent;
    double optOnePercent;
    double total;
    private QuestionResponse questionResponse=new QuestionResponse();
    private static final String TAG = LandingActivity.class.getSimpleName();
    DecimalFormat df = new DecimalFormat("#.##");
    ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_questions_activity);

        initializeViews();

        //SET SURVEY NAME
        surveyNameBtn.setText(LandingActivity.selectedSurveyName);

        //api call to get the questions related to survey
        PostSurvey postSurvey=new PostSurvey(LandingActivity.selectedSurveyId);



        final Call<List<Question>> call=apiService.getQuestionsOfSurvey(postSurvey);

        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                questionsList=response.body();
                Log.d(TAG,"quest count :"+questionsList.size());
                if(questionsList.size()>0) {
                    renderQuestionToUI(count);
                }else{
                    Intent intent=new Intent(SurveyQuestionsActivity.this,LandingActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                Log.e(TAG,"API call failed");
            }
        });


        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(count+1<questionsList.size()){
                        count=count+1;
                        renderQuestionToUI(count);
                    }else{
                        nextBtn.setVisibility(View.GONE);
                    }
            }
        });

        radioOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1) {
                    RadioButton radioButton = (RadioButton) findViewById(checkedId);
                    Question question = questionsList.get(count);
                    questionResponse.setQuestionId(question.getId());
                    for (int i = 0; i < question.getOptionsList().size(); i++) {
                        if (question.getOptionsList().get(i).getOption().equals(radioButton.getText())) {
                            List<SelectedOptions> selectedOptionsList = new ArrayList<SelectedOptions>();
                            SelectedOptions selectedOptions = new SelectedOptions();
                            selectedOptions.setOptionId(question.getOptionsList().get(i).getId());
                            selectedOptionsList.add(selectedOptions);

                            questionResponse.setSelectedOptions(selectedOptionsList);

                        }
                    }
                    Log.d(TAG, "DATA to sent is " + questionResponse.toString());
                }
            }

        });



        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateInput()){
                    //code to post the user response of perticular question
                    final Call<Question> call=apiService.updateQuestionResponse(questionResponse);

                    call.enqueue(new Callback<Question>() {
                        @Override
                        public void onResponse(Call<Question> call, Response<Question> response) {
                            Log.d(TAG,"Question response is sent successfully to server");

                            count = count + 1;
                            Log.d(TAG,"COUNT :"+count);
                            if(count<questionsList.size()) {
                                //count = count + 1;
                                renderQuestionToUI(count);
                            }else{
                                nextBtn.setVisibility(View.GONE);
                            }

                            if(count==questionsList.size()){
                                Intent intent=new Intent(SurveyQuestionsActivity.this,LandingActivity.class);
                                startActivity(intent);
                            }

                        }

                        @Override
                        public void onFailure(Call<Question> call, Throwable t) {
                            Log.e(TAG,"Error while sending question response to server");
                        }
                    });
                }

            }
        });


    }

    private boolean validateInput() {

        if(radioOption1.isChecked() || radioOption2.isChecked()){
            return true;
        }else {
            return false;
        }

    }

    /**
     * helper method used to render the
     * question to UI
     */
    private void renderQuestionToUI(int count) {
      /*  if(count==questionsList.size()-1){
            nextBtn.setVisibility(View.GONE);
        }*/
        //radioOption1.setChecked(false);
        //radioOption2.setChecked(false);
        radioOptions.clearCheck();

        questionTV.setText(questionsList.get(count).getQuestion());

        radioOption1.setText(questionsList.get(count).getOptionsList().get(0).getOption());
        optionOneResultTV.setText(questionsList.get(count).getOptionsList().get(0).getOption()+" "+"%");

        radioOption2.setText(questionsList.get(count).getOptionsList().get(1).getOption());
        optionTwoResultTV.setText(questionsList.get(count).getOptionsList().get(1).getOption()+" "+"%");

         total=questionsList.get(count).getOptionsList().get(0).getResultCount()+questionsList.get(count).getOptionsList().get(1).getResultCount();
        totalVotesTV.setText(" ");
        totalVotesTV.setText("Total Votes :"+" "+total);

        if(questionsList.get(count).getOptionsList().get(0).getResultCount()!=0) {
             optOnePercent = ((questionsList.get(count).getOptionsList().get(0).getResultCount()) / total) * 100;
             optionOneResultTV.setText(" ");

             optionOneResultTV.append(questionsList.get(count).getOptionsList().get(0).getOption()+" "+"%"+Double.valueOf(df.format(optOnePercent)));
        }else{
            optionOneResultTV.setText(" ");
            optionOneResultTV.append(questionsList.get(count).getOptionsList().get(0).getOption()+" "+"%"+0.0);
        }

        if(questionsList.get(count).getOptionsList().get(1).getResultCount()!=0) {
             optTwoPercent = ((questionsList.get(count).getOptionsList().get(1).getResultCount()) / total) * 100;
             optionTwoResultTV.setText(" ");

             optionTwoResultTV.append(questionsList.get(count).getOptionsList().get(1).getOption()+" "+"%"+Double.valueOf(df.format(optTwoPercent)));
        }else{
            optionTwoResultTV.setText(" ");
            optionTwoResultTV.append(questionsList.get(count).getOptionsList().get(1).getOption()+" "+"%"+0.0);
        }




    }


    private void initializeViews() {
        totalVotesTV= (TextView) findViewById(R.id.totalVotesTV);
        questionTV= (TextView) findViewById(R.id.questionTV);
        surveyNameBtn= (Button) findViewById(R.id.surveyNameBtn);
        submitBtn= (Button) findViewById(R.id.submitBtn);
        nextBtn= (Button) findViewById(R.id.nextBtn);
        radioOptions= (RadioGroup) findViewById(R.id.radioOptions);
        radioOption1= (RadioButton) findViewById(R.id.radioOption1);
        optionOneResultTV= (TextView) findViewById(R.id.optionOneResultTV);
        radioOption2= (RadioButton) findViewById(R.id.radioOption2);
        optionTwoResultTV= (TextView) findViewById(R.id.optionTwoResultTV);

    }


}
